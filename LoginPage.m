//
//  LoginPage.m
//  MadMath
//
//  Created by Jermin Bazazian on 9/10/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "LoginPage.h"

@implementation LoginPage

@synthesize loginButton = _loginButton;
@synthesize isneedtoplay;

- (IBAction)performLogin:(id)sender 
{
    if([GlobalFunction NetworkStatus]){
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate openSessionWithAllowLoginUI:YES];
        appDelegate.ispause = YES;
    }else{
       [self.view makeToast:@"Please connect to internet!" duration:(0.3) position:@"center"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_4in.png"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    }
    
    if(self.isneedtoplay)
        [GlobalFunction playbgmusic:@"bgmusic":@"mp3":-1];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated 
{
    
}

- (void) viewWillDisappear:(BOOL)animated
{
    
}

- (void)dealloc {
    [_loginButton release];
    
    [super dealloc];
}

@end
