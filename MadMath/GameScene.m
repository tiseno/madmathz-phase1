//
//  GameScene.m
//  MadMath
//
//  Created by Zack Loi on 9/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene
@synthesize numberview, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, image11, image12, image13, image14, image15, image16, star, star2, star3, star4, star5, star6, detailview, answer, completed, posibility, time, answerlabel, completedlabel, posibilitylabel, timelabel, username, userpoints, userimage, resulttext, imageData, userID, userview, userName, userScore, allimagenumbers;
@synthesize gamemode, gameplayer,currentanswer, positionvalue, anws, totalPoss, gameID, splayerID, splayerUsername, fcorrect, fcompleteTime, fstrRepeat, fstrWrong, fimagedata, match_id;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default_4in.png"]];
        
        self.detailview.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width -self.detailview.frame.size.width) / 2, (self.numberview.frame.origin.y - self.detailview.frame.size.height) + 20, self.detailview.frame.size.width, self.detailview.frame.size.height);
        
        self.numberview.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width -self.numberview.frame.size.width) / 2, ([[UIScreen mainScreen] bounds].size.height - self.numberview.frame.size.height - 40), self.numberview.frame.size.width, self.numberview.frame.size.height);
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default.png"]];
    }
    
    self.userview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_player.png"]];
    
    self.numberview.backgroundColor = [UIColor clearColor];
    
    self.detailview.backgroundColor = [UIColor clearColor];
    
    [GlobalFunction playbgmusic:@"gamecountdown":@"wav":-1];
    self.userpoints.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.username.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    
    self.userpoints.text = [NSString stringWithFormat:@"You have %@ points", self.userScore];
    self.username.text = self.userName;
    
    self.userimage.layer.masksToBounds = YES;
    self.userimage.layer.cornerRadius = 5.0;
    self.userimage.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0].CGColor;
    self.userimage.layer.borderWidth = 1.5;
    
    self.answer.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.posibility.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.time.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.completed.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    
    self.answer.font = [UIFont fontWithName:@"Adonais" size:17];
    self.posibility.font = [UIFont fontWithName:@"Adonais" size:17];
    self.time.font = [UIFont fontWithName:@"Adonais" size:40];
    self.completed.font = [UIFont fontWithName:@"Adonais" size:17];
    self.answerlabel.font = [UIFont fontWithName:@"Adonais" size:17];
    self.posibilitylabel.font = [UIFont fontWithName:@"Adonais" size:17];
    self.timelabel.font = [UIFont fontWithName:@"Adonais" size:12];
    self.completedlabel.font = [UIFont fontWithName:@"Adonais" size:17];
    
    [self.userimage setImage:[UIImage imageWithData:imageData]];
    
    if(self.splayerID != nil)
    {
        NSString *strurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", self.splayerID];
        
        NSURL *url = [NSURL URLWithString:strurl];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            self.fimagedata = [NSData dataWithContentsOfURL:url];
        });
    }
    
    [self loaddata];
    [self changeimage:@"default"];
    [self addhandler];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isvalid = YES;
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
    timer = [NSTimer scheduledTimerWithTimeInterval:(1) target:self selector:@selector(updatetime) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [GlobalFunction stopbgmusic];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isvalid = NO;
    [timer invalidate];
    timer = nil;
    [connectiontimer invalidate];
}

- (void)dealloc {
    [numberview release];
    [image1 release];
    [image2 release];
    [image3 release];
    [image4 release];
    [image5 release];
    [image6 release];
    [image7 release];
    [image8 release];
    [image9 release];
    [image10 release];
    [image11 release];
    [image12 release];
    [image13 release];
    [image14 release];
    [image15 release];
    [image16 release];
    
    [star release];
    [star2 release];
    [star3 release];
    [star4 release];
    [star5 release];
    [star6 release];
    
    [detailview release];
    [answer release];
    [posibility release];
    [time release];
    [completed release];
    [answerlabel release];
    [posibilitylabel release];
    [timelabel release];
    [completedlabel release];
    [userview release];
    [username release];
    [userpoints release];
    [userimage release];
    [resulttext release];
    
    /*ischose = nil;
    isrepeat = nil;
    
    self.userID = nil;
    self.userName = nil;
    self.userScore = nil;
    self.imageData = nil;
    self.allimagenumbers = nil;
    self.anws = nil;
    self.totalPoss = nil;
    
    self.splayerID = nil;
    self.splayerUsername = nil;
    
    self.fcorrect = nil;
    self.fstrWrong = nil;
    self.fstrRepeat = nil;
    self.fcompleteTime = nil;
    self.fimagedata = nil;*/
    
    [ischose release];
    [isrepeat release];
    
    [super dealloc];
}

-(void)checkingconnection{
    if([GlobalFunction NetworkStatus] && !isconnected){
        countdown = 0;
        isconnected = YES;
        isstopped = NO;
        [GlobalFunction resumebgmusic];
        [GlobalFunction RemovingScreen:self];
        timer = [NSTimer scheduledTimerWithTimeInterval:(1) target:self selector:@selector(updatetime) userInfo:nil repeats:YES];
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction LoadingScreen:self];
        isconnected = NO;
    }else if(![GlobalFunction NetworkStatus] && !isconnected){
        if(!isstopped){
            [timer invalidate];
            [GlobalFunction pausebgmusic];
        }
        isstopped = YES;
        countdown++;
        if(countdown > 1200){
            LoginPage *GtestClasssViewController=[[[LoginPage alloc] initWithNibName:@"LoginPage"  bundle:nil] autorelease];
            GtestClasssViewController.isneedtoplay = YES;
            [self presentModalViewController:GtestClasssViewController animated:YES];
        }
    }
    
    if([GlobalFunction NetworkStatus])
    {
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        if (appDelegate.ispause)
        {
            if(!ispause){
                [timer invalidate];
                [GlobalFunction pausebgmusic];
                [GlobalFunction IncomingCallLoadingScreen:self];
                ispause = YES;
            }
        }else {
            if(ispause){
                [GlobalFunction IncomingCallRemovingScreen:self];
                timer = [NSTimer scheduledTimerWithTimeInterval:(1) target:self selector:@selector(updatetime) userInfo:nil repeats:YES];
                ispause = NO;
                [GlobalFunction resumebgmusic];
            }
        }
        
        if(appDelegate.isquit){
            [timer invalidate];
            timer = nil;
            if(self.gameplayer == 2){
                WebServices *ws = [[WebServices alloc] init];
                [ws insertSPlayerDetails:@"I quitted" :@"" :[time.text intValue] :correct :0 :wrong :self.match_id :repeat];
                [ws release];
            }
        }
        
        if(![GlobalFunction isplay]){
            int timeleft = [time.text intValue];
            if(timeleft > 0 && ![posibility.text isEqualToString:completed.text] && !appDelegate.ispause)
                [GlobalFunction resumebgmusic];
        }
    }
}

-(void) loaddata{
    ischose = [[NSMutableArray alloc]init];
    for(int i = 0; i< 16; i++)
        [ischose addObject:[NSNumber numberWithBool:NO]];
    isrepeat = [[NSMutableArray alloc]init];
    
    time.text = @"60";
    self.answer.text = self.anws;
    self.posibility.text = self.totalPoss;
    self.positionvalue = 0;
    self.currentanswer = 0;
    correct = 0;
    repeat = 0;
    wrong = 0;
    
    
    star.hidden = YES;
    star2.hidden = YES;
    star3.hidden = YES;
    star4.hidden = YES;
    star5.hidden = YES;
    star6.hidden = YES;
    
    star1Initial=star.transform;
    star2Initial=star2.transform;
    star3Initial=star3.transform;
    star4Initial=star4.transform;
    star5Initial=star5.transform;
    star6Initial=star6.transform;
    
    isconnected = YES;
    isstopped = NO;
    ispause = NO;
    resulttext.hidden = YES;
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.ispause = NO;
    appDelegate.isquit = NO;
    appDelegate.isvalid = YES;
}

-(void)addhandler
{
    UIPanGestureRecognizer *Recognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(SwipeHandle:)] autorelease];
    Recognizer.maximumNumberOfTouches = 1;
    Recognizer.minimumNumberOfTouches = 1;
    [self.numberview addGestureRecognizer:Recognizer];
}

-(void)changeimage:(NSString *)status{
    if([status isEqualToString:@"default"])
    {
        for(int i = 1; i <=16;i++)
        {
            UIImageView *imageview = (UIImageView*)[self.numberview viewWithTag:i];
            imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"stone%@", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
            [image1 setUserInteractionEnabled:YES];
        }
    }
    else if([status isEqualToString:@"correct"] || [status isEqualToString:@"selected"])
    {
        for(int i = 1; i<= ischose.count; i++)
        {
            if([[ischose objectAtIndex:i-1] boolValue] == 1)
            {
                UIImageView *imageview = (UIImageView*)[self.numberview viewWithTag:i];
                imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"stone%@_correct", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
            }
        }
    }
    else if([status isEqualToString:@"wrong"] || [status isEqualToString:@"repeat"])
    {
        for(int i = 1; i<= ischose.count; i++)
        {
            if([[ischose objectAtIndex:i-1] boolValue] == 1)
            {
                UIImageView *imageview = (UIImageView*)[self.numberview viewWithTag:i];
                imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"stone%@_wrong_repeat", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
            }
        }
    }
}

-(void)displayresulttext:(NSString *)status{
    resulttext.hidden = NO;
    if([status isEqualToString:@"correct"] )
    {
        resulttext.image = [UIImage imageNamed:@"correct.png"];
        resulttext.frame = CGRectMake((self.numberview.frame.size.width - resulttext.image.size.width) / 2, (self.numberview.frame.size.height - resulttext.image.size.height) / 2, resulttext.image.size.width, resulttext.image.size.height);
    }
    else if([status isEqualToString:@"wrong"])
    {
        resulttext.image = [UIImage imageNamed:@"wrong.png"];
        resulttext.frame = CGRectMake((self.numberview.frame.size.width - resulttext.image.size.width) / 2, (self.numberview.frame.size.height - resulttext.image.size.height) / 2, resulttext.image.size.width, resulttext.image.size.height);
    }
    else if([status isEqualToString:@"repeat"])
    {
        resulttext.image = [UIImage imageNamed:@"repeat.png"];
        resulttext.frame = CGRectMake((self.numberview.frame.size.width - resulttext.image.size.width) / 2, (self.numberview.frame.size.height - resulttext.image.size.height) / 2, resulttext.image.size.width, resulttext.image.size.height);
    }
    
    resulttext.alpha = 0.0f;
    [UIView beginAnimations:nil context:nil];
    
    [UIView setAnimationDuration:0.3];
    
    [UIView animateWithDuration:0.0 animations:^{
        resulttext.alpha = 1.0f;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             resulttext.hidden = YES;
                             [self performSelector:@selector(changeimage:) withObject:@"default" afterDelay:0.1];
                         }
                     }];
    
    [UIView commitAnimations];
}

-(void)transitionstar{
    star.hidden = NO;
    star2.hidden = NO;
    star3.hidden = NO;
    star4.hidden = NO;
    star5.hidden = NO;
    star6.hidden = NO;
    
    star.transform=star1Initial;
    star2.transform=star2Initial;
    star3.transform=star3Initial;
    star4.transform=star4Initial;
    star5.transform=star5Initial;
    star6.transform=star6Initial;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3f];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1, 1);
        CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI);
        CGAffineTransform rotate2 = CGAffineTransformMakeRotation(-M_PI);
        CGAffineTransform rotate3 = CGAffineTransformMakeRotation(-M_PI);
        CGAffineTransform rotate4 = CGAffineTransformMakeRotation(M_PI);
        CGAffineTransform rotate5 = CGAffineTransformMakeRotation(-M_PI);
        CGAffineTransform rotate6 = CGAffineTransformMakeRotation(M_PI);
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-800, -800);
        CGAffineTransform translate2 = CGAffineTransformMakeTranslation(600, -600);
        CGAffineTransform translate3 = CGAffineTransformMakeTranslation(400, 400);
        CGAffineTransform translate4 = CGAffineTransformMakeTranslation(-200, 200);
        CGAffineTransform translate5 = CGAffineTransformMakeTranslation(900, -300);
        CGAffineTransform translate6 = CGAffineTransformMakeTranslation(-200, 500);
        
        CGAffineTransform transform =  CGAffineTransformConcat(translate, scale);
        transform = CGAffineTransformConcat(transform, rotate);
        
        CGAffineTransform transform2 =  CGAffineTransformConcat(translate2, scale);
        transform2 = CGAffineTransformConcat(transform2, rotate2);
        
        CGAffineTransform transform3 =  CGAffineTransformConcat(translate3, scale);
        transform3 = CGAffineTransformConcat(transform3, rotate3);
        
        CGAffineTransform transform4 =  CGAffineTransformConcat(translate4, scale);
        transform4 = CGAffineTransformConcat(transform4, rotate4);
        CGAffineTransform transform5 =  CGAffineTransformConcat(translate5, scale);
        transform5 = CGAffineTransformConcat(transform5, rotate5);
        CGAffineTransform transform6 =  CGAffineTransformConcat(translate6, scale);
        transform6 = CGAffineTransformConcat(transform6, rotate6);
        
        star.transform = transform;
        star2.transform = transform2;
        star3.transform = transform3;
        star4.transform = transform4;
        star5.transform = transform5;
        star6.transform = transform6;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             star.hidden = YES;
                             star2.hidden = YES;
                             star3.hidden = YES;
                             star4.hidden = YES;
                             star5.hidden = YES;
                             star6.hidden = YES;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)updatetime{
    int timeleft = [time.text intValue];
    if(timeleft >= 0){
        if(timeleft == 0){
            [self result];
        }
        else if(timeleft > 1 && timeleft <= 11)
        {
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            if(!appDelegate.ismute)
            {
                soundeffect = [GlobalFunction createSoundID: @"alert"];
                AudioServicesPlaySystemSound(soundeffect);
            }
            timeleft--;
            time.text = [NSString stringWithFormat:@"%d", timeleft];
            [self textblinkcontroller];
                [self performSelector:@selector(textblinkcontroller) withObject:nil afterDelay:0.4];
        }
        else{
            timeleft--;
            time.text = [NSString stringWithFormat:@"%d", timeleft];
        }
    }
}

-(void)textblinkcontroller
{
    [self performSelector:@selector(textblinkingred) withObject:nil afterDelay:0.2];
}

-(void)textblinkingred
{
    time.textColor = [UIColor colorWithRed:220.0/255.0 green:60.0/255.0 blue:11.0/255.0 alpha:1.0];
    [self performSelector:@selector(textblinkingori) withObject:nil afterDelay:0.2];
}

-(void)textblinkingori
{
    time.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
}

- (void)result{
    if([time.text isEqualToString:@"0"]){
        [GlobalFunction stopbgmusic];
        UIView *covered = [[[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)] autorelease];
        covered.backgroundColor = [UIColor clearColor];
        [self.view addSubview:covered];
        
        if(self.gamemode == 1){
            SingleMatchResultScene *GtestClasssViewController=[[[SingleMatchResultScene alloc] initWithNibName:@"SingleMatchResultScene"  bundle:nil] autorelease];
            GtestClasssViewController.userName = self.userName;
            GtestClasssViewController.imageData = self.imageData;
            GtestClasssViewController.userID = self.userID;
            GtestClasssViewController.userScore = self.userScore;
            
            if([completed.text isEqualToString:posibility.text])
                GtestClasssViewController.iswin = YES;
            else
                GtestClasssViewController.iswin = NO;
            [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
        }else{
            if(self.gameplayer == 1){
                BattleMatchWaitingScene *GtestClasssViewController=[[[BattleMatchWaitingScene alloc] initWithNibName:@"BattleMatchWaitingScene"  bundle:nil] autorelease];
                GtestClasssViewController.userName = self.userName;
                GtestClasssViewController.imageData = self.imageData;
                GtestClasssViewController.userID = self.userID;
                GtestClasssViewController.userScore = self.userScore;
                
                GtestClasssViewController.correct = [NSString stringWithFormat:@"%d", correct];
                GtestClasssViewController.wrong = [NSString stringWithFormat:@"%d", wrong];
                GtestClasssViewController.repeat = [NSString stringWithFormat:@"%d", repeat];
                GtestClasssViewController.completeTime = [NSString stringWithFormat:@"%d", 60];
                GtestClasssViewController.gameID = self.gameID;
                GtestClasssViewController.splayerID = self.splayerID;
                GtestClasssViewController.splayerUsername = self.splayerUsername;
                
                [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
            }else {
                BattleMatchResultScene *GtestClasssViewController=[[[BattleMatchResultScene alloc] initWithNibName:@"BattleMatchResultScene"  bundle:nil] autorelease];
                GtestClasssViewController.userName = self.userName;
                GtestClasssViewController.imageData = self.imageData;
                GtestClasssViewController.userID = self.userID;
                GtestClasssViewController.userScore = self.userScore;
                
                GtestClasssViewController.correct = [NSString stringWithFormat:@"%d", correct];
                GtestClasssViewController.strWrong = [NSString stringWithFormat:@"%d", wrong];
                GtestClasssViewController.strRepeat = [NSString stringWithFormat:@"%d", repeat];
                GtestClasssViewController.completeTime = [NSString stringWithFormat:@"%d", 60];
                
                GtestClasssViewController.splayerUsername = self.splayerUsername;
                GtestClasssViewController.splayerID = self.splayerID;
                
                GtestClasssViewController.fstrWrong = self.fstrWrong;
                GtestClasssViewController.fstrRepeat = self.fstrRepeat;
                GtestClasssViewController.fcompleteTime = self.fcompleteTime;
                GtestClasssViewController.fcorrect = self.fcorrect;
                GtestClasssViewController.fimagedata = self.fimagedata;
                GtestClasssViewController.match_id = self.match_id;
                
                [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
            }
        }
    }else {
        if(self.gamemode == 1){
            if([completed.text isEqualToString:posibility.text]){
                [GlobalFunction stopbgmusic];
                SingleMatchResultScene *GtestClasssViewController=[[[SingleMatchResultScene alloc] initWithNibName:@"SingleMatchResultScene"  bundle:nil] autorelease];
                GtestClasssViewController.userName = self.userName;
                GtestClasssViewController.imageData = self.imageData;
                GtestClasssViewController.userID = self.userID;
                GtestClasssViewController.userScore = self.userScore;
                
                GtestClasssViewController.iswin = YES;
                [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
            }
        }else{
            [GlobalFunction stopbgmusic];
            if([completed.text isEqualToString:posibility.text]){
                if(self.gameplayer == 1){
                    BattleMatchWaitingScene *GtestClasssViewController=[[[BattleMatchWaitingScene alloc] initWithNibName:@"BattleMatchWaitingScene"  bundle:nil] autorelease];
                    GtestClasssViewController.userName = self.userName;
                    GtestClasssViewController.imageData = self.imageData;
                    GtestClasssViewController.userID = self.userID;
                    GtestClasssViewController.userScore = self.userScore;
                    
                    GtestClasssViewController.correct = [NSString stringWithFormat:@"%d", correct];
                    GtestClasssViewController.wrong = [NSString stringWithFormat:@"%d", wrong];
                    GtestClasssViewController.repeat = [NSString stringWithFormat:@"%d", repeat];
                    GtestClasssViewController.completeTime = [NSString stringWithFormat:@"%d", 60 - [time.text intValue]];
                    GtestClasssViewController.gameID = self.gameID;
                    GtestClasssViewController.splayerID = self.splayerID;
                    GtestClasssViewController.splayerUsername = self.splayerUsername;
                    
                    [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
                }else {
                    BattleMatchResultScene *GtestClasssViewController=[[[BattleMatchResultScene alloc] initWithNibName:@"BattleMatchResultScene"  bundle:nil] autorelease];
                    GtestClasssViewController.userName = self.userName;
                    GtestClasssViewController.imageData = self.imageData;
                    GtestClasssViewController.userID = self.userID;
                    GtestClasssViewController.userScore = self.userScore;
                    
                    GtestClasssViewController.correct = [NSString stringWithFormat:@"%d", correct];
                    GtestClasssViewController.strWrong = [NSString stringWithFormat:@"%d", wrong];
                    GtestClasssViewController.strRepeat = [NSString stringWithFormat:@"%d", repeat];
                    GtestClasssViewController.completeTime = [NSString stringWithFormat:@"%d", 60 - [time.text intValue]];
                    
                    GtestClasssViewController.splayerUsername = self.splayerUsername;
                    GtestClasssViewController.splayerID = self.splayerID;
                    
                    GtestClasssViewController.fstrWrong = self.fstrWrong;
                    GtestClasssViewController.fstrRepeat = self.fstrRepeat;
                    GtestClasssViewController.fcompleteTime = self.fcompleteTime;
                    GtestClasssViewController.fcorrect = self.fcorrect;
                    GtestClasssViewController.fimagedata = self.fimagedata;
                    GtestClasssViewController.match_id = self.match_id;
                    
                    [self performSelector:@selector(delay:) withObject:GtestClasssViewController afterDelay:0.3];
                }
            }
        }
    }
}

-(void)delay:(UIViewController *)viewcontroller{
    [self presentModalViewController:viewcontroller animated:YES];
}

-(int)converter:(NSString*)number{
    
    return [number intValue];
}

-(void)countinganswer{
    if(isbegan && isended)
    {
        BOOL positionrepeat = NO;
        if(currentanswer == [answer.text intValue])
        {
            for(int i = 0; i< isrepeat.count; i++){
                if(positionvalue == [[isrepeat objectAtIndex:i] intValue])
                {
                    positionrepeat = YES;
                    repeat++;
                    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                    if(!appDelegate.ismute){
                        soundeffect = [GlobalFunction createSoundID: @"wrongrepeat"];
                        AudioServicesPlaySystemSound(soundeffect);
                    }
                    [self changeimage:@"repeat"];
                    [self displayresulttext:@"repeat"];
                    break;
                }
                else
                {
                    continue;
                }
            }
            
            if(!positionrepeat)
            {
                [isrepeat addObject:[NSNumber numberWithInt:positionvalue]];
                AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                if(!appDelegate.ismute){
                    soundeffect = [GlobalFunction createSoundID: @"correct"];
                    AudioServicesPlaySystemSound(soundeffect);
                }
                [self displayresulttext:@"correct"];
                [self transitionstar];
                correct++;
                completed.text = [NSString stringWithFormat:@"%d", correct];
                [self result];
            }
        }
        else
        {
            wrong++;
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            if(!appDelegate.ismute){
                soundeffect = [GlobalFunction createSoundID: @"wrongrepeat"];
                AudioServicesPlaySystemSound(soundeffect);
            }
            [self changeimage:@"wrong"];
            [self displayresulttext:@"wrong"];
        }
        isbegan = NO;
        isended = NO;
        currentanswer = 0;
        positionvalue = 0;
        for(int i = 0; i< 16; i++)
            [ischose replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
    }
}

/*- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchLocation = [touch locationInView:self.numberview];
    if(!ispause && [time.text intValue] > 0){
        if(!isbegan && !isended){
            for(int i = 1; i <= 16; i++)
            {
                UIView *imageview = [self.numberview viewWithTag:i];
                if (CGRectContainsPoint(imageview.frame, currentTouchLocation))
                {
                    isbegan = YES;
                    isreverse = NO;
                    if([[ischose objectAtIndex:i-1] boolValue] == 0)
                    {
                        currentposition = imageview.tag;
                        positionvalue += pow(i, 3);
                        currentanswer += [self converter:[NSString stringWithFormat:@"%@", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
                        [ischose replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                        [self changeimage:@"selected"];
                    }
                    break;
                }
            }
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesMoved:touches withEvent:event];
    
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchLocation = [touch locationInView:self.numberview];
    
    float gapX = image1.frame.size.width / 8;
    float gapY = image1.frame.size.height / 8;
    
    if(isbegan && !isended)
    {
        if(currentTouchLocation.x >= 0 && currentTouchLocation.x <= self.numberview.frame.size.width && currentTouchLocation.y >= 0 && currentTouchLocation.y <= self.numberview.frame.size.height)
        {
            for(int i = 1; i <= 16; i++)
            {
                UIView *imageview = [self.numberview viewWithTag:i];
                if (CGRectContainsPoint(imageview.frame, currentTouchLocation))
                {
                    if((currentTouchLocation.x >= imageview.frame.origin.x + gapX && currentTouchLocation.x < imageview.frame.origin.x + imageview.frame.size.width  - gapX) && (currentTouchLocation.y >= imageview.frame.origin.y  + gapY && currentTouchLocation.y < imageview.frame.origin.y + imageview.frame.size.height - gapY ))
                    {
                        if([[ischose objectAtIndex:i-1] boolValue] == 0 && !isreverse)
                        {
                            currentposition = imageview.tag;
                            positionvalue += pow(i, 3);
                            currentanswer += [self converter:[NSString stringWithFormat:@"%@", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
                            [ischose replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                            [self changeimage:@"selected"];
                        }
                        else
                        {
                            if(currentposition != imageview.tag)
                            {
                                isreverse = YES;
                            }
                            else
                            {
                                isreverse = NO;
                            }
                        }
                        break;
                    }
                }
            }
        }
        else
        {
            isended = YES;
            isoutofbound = YES;
            if(isbegan && isoutofbound)
                [self countinganswer];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    if(!isoutofbound)
    {
        isended = YES;
        [self countinganswer];
    }
    else
        isoutofbound = NO;
}*/

- (void)SwipeHandle:(UIPanGestureRecognizer*)gestureRecognizer
{
    UIView *theSuperview = self.numberview;
    CGPoint touchPointInSuperview = [gestureRecognizer locationInView:theSuperview];
    
    float gapX = image1.frame.size.width / 8;
    float gapY = image1.frame.size.height / 8.48;
    
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        if(!ispause && [time.text intValue] > 0){
            if(!isbegan && !isended){
                for(int i = 1; i <= 16; i++)
                {
                    UIView *imageview = [self.numberview viewWithTag:i];
                    if (CGRectContainsPoint(imageview.frame, touchPointInSuperview))
                    {
                        isbegan = YES;
                        isreverse = NO;
                        if([[ischose objectAtIndex:i-1] boolValue] == 0)
                        {
                            currentposition = imageview.tag;
                            positionvalue += pow(i, 3);
                            currentanswer += [self converter:[NSString stringWithFormat:@"%@", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
                            [ischose replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                            [self changeimage:@"selected"];
                        }
                        
                        break;
                    }
                }
            }
        }
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        if(isbegan && !isended)
        {
            if(touchPointInSuperview.x >= 0 && touchPointInSuperview.x <= self.numberview.frame.size.width && touchPointInSuperview.y >= 0 && touchPointInSuperview.y <= self.numberview.frame.size.height)
            {
                for(int i = 1; i <= 16; i++)
                {
                    UIImageView *imageview = (UIImageView*)[self.numberview viewWithTag:i];
                    if (CGRectContainsPoint(imageview.frame, touchPointInSuperview))
                    {
                        if((touchPointInSuperview.x >= imageview.frame.origin.x + gapX && touchPointInSuperview.x < imageview.frame.origin.x + imageview.frame.size.width  - gapX) && (touchPointInSuperview.y >= imageview.frame.origin.y  + gapY && touchPointInSuperview.y < imageview.frame.origin.y + imageview.frame.size.height - gapY ))
                        {
                            if([[ischose objectAtIndex:i-1] boolValue] == 0 && !isreverse)
                            {
                                currentposition = imageview.tag;
                                positionvalue += pow(i, 3);
                                currentanswer += [self converter:[NSString stringWithFormat:@"%@", [allimagenumbers substringWithRange:NSMakeRange(i-1, 1)]]];
                                [ischose replaceObjectAtIndex:i-1 withObject:[NSNumber numberWithBool:YES]];
                                [self changeimage:@"selected"];
                            }
                            else
                            {
                                if(currentposition != imageview.tag)
                                {
                                    isreverse = YES;
                                }
                                else
                                {
                                    isreverse = NO;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            else
            {
                isended = YES;
                isoutofbound = YES;
                if(isbegan && isoutofbound)
                    [self countinganswer];
            }
        }
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        if(!isoutofbound)
        {
            isended = YES;
            [self countinganswer];
        }
        else
            isoutofbound = NO;
    }
}

@end
