//
//  UserProfilePage.m
//  MadMath
//
//  Created by Jermin Bazazian on 9/10/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#define TableRowHeight 72

#import "UserProfilePage.h"

@implementation UserProfilePage

@synthesize svController;
@synthesize titleimageview;
@synthesize tblChallenge;
@synthesize tblResult;
@synthesize userScore;
@synthesize lblScore;
@synthesize imgUser;
@synthesize fbfriendlist = _fbfriendlist;
@synthesize userList = _userList;
@synthesize matchedUser = _matchedUser;
@synthesize userID;
@synthesize userName;
@synthesize imageData;
@synthesize lblWelcome;
@synthesize challengeResult;
@synthesize challengeList;
@synthesize isneedtoplay;

@synthesize guideview, guidescroll, guidebutton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default_4in.png"]];
        self.titleimageview.frame = CGRectMake(22, 12, 277, 63);
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default.png"]];
    }
    
    if(self.isneedtoplay)
        [GlobalFunction playbgmusic:@"bgmusic":@"mp3":-1];
    
    self.tblChallenge.hidden = YES;
    self.tblResult.hidden = YES;
    
    [GlobalFunction LoadingScreen:self];
    isconnected = YES;
    isneedtorefresh = YES;
    
    self.svController.backgroundColor = [UIColor clearColor];
    
    self.imgUser.layer.masksToBounds = YES;
    self.imgUser.layer.cornerRadius = 5.0;
    self.imgUser.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0].CGColor;
    self.imgUser.layer.borderWidth = 1.5;
    
    userID = [[NSString alloc] init];
    userName = [[NSString alloc] init];
    userScore = [[NSString alloc] init];
    
    self.lblWelcome.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    [self performSelector:@selector(delay) withObject:nil afterDelay:0.1];
    
    self.guideview.frame = CGRectMake(-[[UIScreen mainScreen] bounds].size.width, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
    guidetransform = self.guideview.transform;
    self.guideview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:self.guideview];
    
    UISwipeGestureRecognizer *rightRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandle:)] autorelease];
    rightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [rightRecognizer setNumberOfTouchesRequired:1];
    [self.guidescroll addGestureRecognizer:rightRecognizer];
    
    UISwipeGestureRecognizer *leftRecognizer = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandle:)] autorelease];
    leftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [leftRecognizer setNumberOfTouchesRequired:1];
    [self.guidescroll addGestureRecognizer:leftRecognizer];
    guidepage = 1;
    
    self.guidebutton.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - self.guidebutton.frame.size.width) / 2, [[UIScreen mainScreen] bounds].size.height - self.guidebutton.frame.size.height - (([[UIScreen mainScreen] bounds].size.height - self.guidescroll.frame.size.height - 20) - self.guidebutton.frame.size.height) / 2, self.guidebutton.frame.size.width, self.guidebutton.frame.size.height);
    
    for(int i = 1; i<= 5; i++)
    {
        UIImageView *guideimage = [[[UIImageView alloc]initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"guide_%d.png", i]]] autorelease];
        guideimage.frame = CGRectMake(guideimage.frame.size.width * (i - 1), 0, guideimage.image.size.width, guideimage.image.size.height);
        [self.guidescroll addSubview:guideimage];
    }
    self.guidescroll.contentSize = CGSizeMake(280 * 5, 350);
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    countinterrupted = 0;
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(retrieveResult) userInfo:nil repeats:YES];
    connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if(appDelegate.isshow)
        [self performSelector:@selector(guideviewanimationin) withObject:nil afterDelay:0.15];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [refreshTimer invalidate];
    [connectiontimer invalidate];
}

- (void)dealloc
{
    [svController release];
    [tblChallenge release];
    [tblResult release];
    [imgUser release];
    [lblScore release];
    [userID release];
    [userName release];
    [titleimageview release];
    [lblWelcome release];
    
    [guideview release];
    [guidescroll release];
    [guidebutton release];
    
    /*challengeList  = nil;
     challengeResult = nil;
     _fbfriendlist = nil;
     _matchedUser = nil;
     _userList = nil;
     userID = nil;
     userName = nil;
     userScore = nil;
     imageData = nil;*/
    
    [_matchedUser release];
    [_userList release];
    
    [challengeResult release];
    [challengeList release];
    
    [super dealloc];
}

-(void)delay{
    if([GlobalFunction NetworkStatus])
    {
        if (FBSession.activeSession.isOpen)
        {
            [[FBRequest requestForMe] startWithCompletionHandler:
             ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
             {
                 if (!error)
                 {
                     userID = [[userID stringByAppendingString:[NSString stringWithFormat:@"%@",user.id]] retain];
                     
                     userName = [[userName stringByAppendingString: [NSString stringWithFormat:@"%@",user.name]]retain];
                     
                     NSString *strurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", userID];
                     
                     NSURL *url = [NSURL URLWithString:strurl];
                     
                     imageData = [[NSData dataWithContentsOfURL:url] retain];
                     
                     WebServices *ws2 = [[WebServices alloc] init];
                     
                     userScore = [[ws2 selectUserScoreByFBID:userID] retain];
                     
                     AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                     
                     //check if new user
                     if([userScore isEqualToString:@""])
                     {
                         NSArray *personalInfo = [NSArray arrayWithObjects:user.first_name, user.last_name, [user objectForKey:@"gender"], [user objectForKey:@"email"], user.name, userID, appDelegate.tokendevice, nil];
                         
                         [ws2 insertNewUser:personalInfo];
                         
                         self.svController.contentSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
                         
                         userScore = @"0";
                         
                         isfirsttime = YES;
                     }
                     else
                     {
                         isfirsttime = NO;
                         [ws2 updateUserPushToken:appDelegate.tokendevice :userID];
                     }
                     
                     NSString *formatScore = [NSString stringWithFormat:@"You have %@ points", userScore];
                     
                     self.lblScore.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
                     self.lblScore.text = formatScore;
                     
                     [self.imgUser setImage:[UIImage imageWithData:imageData]];
                     
                     /* filter here */
                     
                     NSArray *test = [ws2 selectChallengeResultByFBID:userID];
                     NSMutableArray *test2 = [[NSMutableArray alloc] init];
                     
                     for(int i = 0; i < test.count; i++)
                     {
                         if(test2.count > 0)
                         {
                             NSDictionary* usinfo = [test objectAtIndex:i];
                             NSDictionary* usdetails = [usinfo objectForKey:@"user"];
                             
                             BOOL decider = true;
                             
                             for(int j = 0; j < test2.count; j++)
                             {
                                 NSDictionary* usinfo2 = [test2 objectAtIndex:j];
                                 NSDictionary* usdetails2 = [usinfo2 objectForKey:@"user"];
                                 
                                 if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:[usdetails2 objectForKey:@"fplayer_id"]])
                                 {
                                     if([[usdetails objectForKey:@"splayer_id"] isEqualToString:[usdetails2 objectForKey:@"splayer_id"]])
                                     {
                                         if([[usdetails objectForKey:@"match_id"] intValue] >= [[usdetails2 objectForKey:@"match_id"] intValue])
                                         {
                                             
                                             //remove & add
                                             [test2 removeObjectAtIndex:j];
                                             [test2 addObject:[test objectAtIndex:i]];
                                             
                                             decider = false;
                                             
                                             break;
                                         }
                                         else
                                         {
                                             //since the 1 in array is the latest.. no need to remove & replace
                                         }
                                     }
                                     else
                                     {
                                         //decider = true;
                                         //[test2 addObject:[test objectAtIndex:i]];
                                     }
                                 }
                                 else
                                 {
                                     if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:[usdetails2 objectForKey:@"splayer_id"]])
                                     {
                                         if([[usdetails objectForKey:@"splayer_id"] isEqualToString:[usdetails2 objectForKey:@"fplayer_id"]])
                                         {
                                             if([[usdetails objectForKey:@"match_id"] intValue] >= [[usdetails2 objectForKey:@"match_id"] intValue])
                                             {
                                                 [test2 removeObjectAtIndex:j];
                                                 [test2 addObject:[test objectAtIndex:i]];
                                                 
                                                 decider = false;
                                                 
                                                 break;
                                             }
                                             else
                                             {
                                                 //no replacement
                                             }
                                         }
                                         else
                                         {
                                             //decider = true;
                                             //[test2 addObject:[test objectAtIndex:i]];
                                         }
                                     }
                                     else
                                     {
                                         //decider = true;
                                         //[test2 addObject:[test objectAtIndex:i]];
                                     }
                                 }
                             }
                             
                             if(decider)
                             {
                                 [test2 addObject:[test objectAtIndex:i]];
                             }
                         }
                         else
                         {
                             [test2 addObject:[test objectAtIndex:i]];
                         }
                     }
                     
                     /* filter end */
                     
                     challengeResult = [[test2 copy] retain];
                     
                     challengeList = [[ws2 selectChallengeByFBID:userID] retain];
                     
                     [test2 release];
                     
                     [ws2 release];
                     
                     [GlobalFunction RemovingScreen:self];
                     
                     if(challengeList.count > 0){
                         self.tblChallenge.hidden = NO;
                         self.tblChallenge.frame = CGRectMake(15, 258, 290, 51 + 72 * challengeList.count);
                         if(challengeResult.count != 0){
                             self.tblResult.hidden = NO;
                             self.tblResult.frame = CGRectMake(15, 72 * challengeList.count + 319, 290, 51 + 72 * challengeResult.count);
                             self.svController.contentSize = CGSizeMake(320, 51 * 2 + 72 * (challengeList.count + challengeResult.count) + 278);
                         }
                         else{
                             self.svController.contentSize = CGSizeMake(320, 51 + 72 * challengeList.count + 268);
                         }
                     }else{
                         if(challengeResult.count > 0){
                             self.tblResult.hidden = NO;
                             self.tblResult.frame = CGRectMake(15, 258, 290, 51 + 72 * challengeResult.count);
                             self.svController.contentSize = CGSizeMake(320, 51 + 72 * (challengeList.count + challengeResult.count) + 268);
                         }
                         else{
                             self.svController.contentSize = CGSizeMake(320, 258);
                         }
                     }
                     
                     self.tblResult.scrollEnabled = NO;
                     self.tblChallenge.scrollEnabled = NO;
                     [tblChallenge reloadData];
                     [tblResult reloadData];
                     
                     if(![GlobalFunction isplay])
                         [self.view makeToast:@"Background Music has been interrupted." duration:(0.5) position:@"center"];
                 }
             }];
        }
        
        WebServices *ws = [[WebServices alloc] init];
        _userList = [[ws selectAllUserFBIDAndScore] retain];
        _matchedUser = [[NSMutableArray alloc] init];
        
        [ws release];
        [challengeResult release];
        [challengeList release];
        
        if(isfirsttime)
            [self guideviewanimationin];
    }
}

-(void)checkingconnection{
    if([GlobalFunction NetworkStatus] && !isconnected){
        [GlobalFunction RemovingScreen:self];
        countdown = 0;
        isconnected = YES;
        refreshTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(retrieveResult) userInfo:nil repeats:YES];
    }else if(![GlobalFunction NetworkStatus] && isconnected){
        [GlobalFunction LoadingScreen:self];
        isconnected = NO;
        [refreshTimer invalidate];
    }else if(![GlobalFunction NetworkStatus] && !isconnected){
        countdown++;
        if(countdown > 1200){
            LoginPage *GtestClasssViewController=[[[LoginPage alloc] initWithNibName:@"LoginPage"  bundle:nil] autorelease];
            [self presentModalViewController:GtestClasssViewController animated:YES];
        }
    }
    
    if(![GlobalFunction isplay]){
        ++countinterrupted;
        if(countinterrupted < 4)
            [GlobalFunction resumebgmusic];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == tblResult)
    {
        return challengeResult.count;
    }
    else
    {
        return challengeList.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.layer.borderWidth = 2.5;
    tableView.layer.cornerRadius = 10;
    tableView.layer.borderColor = [UIColor colorWithRed:0.3960 green:0.2627 blue:0.1294 alpha:1.0].CGColor;
    
    
    NSString* tempID = @"";
    
    if(tableView == self.tblChallenge)
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        challengelistcell= (ChallengeListCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(challengelistcell == nil)
        {
            challengelistcell = [[[ChallengeListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        challengelistcell.frame = CGRectMake(0, 0, self.tblChallenge.frame.size.width, TableRowHeight);
        challengelistcell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [tableView setSeparatorColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0]];
        
        if(challengeList != nil || [challengeList count] != 0)
        {
            NSDictionary* usinfo = [challengeList objectAtIndex:indexPath.row];
            
            NSDictionary* usdetails = [usinfo objectForKey:@"user"];
            
            if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:userID])
            {
                challengelistcell.usernamelabel.text = [usdetails objectForKey:@"splayer_username"];
                tempID = [usdetails objectForKey:@"splayer_id"];
                challengelistcell.usermessagelabel.text = [NSString stringWithFormat:@"You had hit %d scores in %@ seconds", [[usdetails objectForKey:@"fplayer_comp_poss"] intValue], [usdetails objectForKey:@"fplayer_comp_time"]];
            }
            else
            {
                challengelistcell.usernamelabel.text = [usdetails objectForKey:@"fplayer_username"];
                tempID = [usdetails objectForKey:@"fplayer_id"];
                challengelistcell.usermessagelabel.text = [NSString stringWithFormat:@"Was hit %d scores in %@ seconds",[[usdetails objectForKey:@"fplayer_comp_poss"] intValue], [usdetails objectForKey:@"fplayer_comp_time"]];
            }
            
            int tempDifferent = [[usdetails objectForKey:@"DiffTime"] intValue];
            
            tempDifferent = tempDifferent / 60;
            
            if(tempDifferent == 0)
            {
                challengelistcell.userplaytimelabel.text = [NSString stringWithFormat:@"%@ seconds ago", [usdetails objectForKey:@"DiffTime"]];
            }
            else
            {
                if(tempDifferent < 2)
                {
                    challengelistcell.userplaytimelabel.text = @"1 minute ago";
                }
                else if(tempDifferent < 60)
                {
                    challengelistcell.userplaytimelabel.text = [NSString stringWithFormat:@"%d minutes ago", tempDifferent];
                }
                else if(tempDifferent > 60)
                {
                    tempDifferent = tempDifferent / 60;
                    
                    if(tempDifferent < 2)
                    {
                        challengelistcell.userplaytimelabel.text = [NSString stringWithFormat:@"%d hour ago", tempDifferent];
                    }
                    else
                    {
                        challengelistcell.userplaytimelabel.text = [NSString stringWithFormat:@"%d hours ago", tempDifferent];
                    }
                }
            }
            
            NSString *strurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", tempID];
            
            NSURL *url = [NSURL URLWithString:strurl];
            
            challengelistcell.userprofileimage.imageURL = url;
            
            if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:userID])
            {
                [challengelistcell.actionbutton setBackgroundImage:[UIImage imageNamed:@"btn_waiting.png"] forState:UIControlStateNormal];
                challengelistcell.actionbutton.userInteractionEnabled = NO;
            }
            else
            {
                challengelistcell.actionbutton.tag = indexPath.row + 100;
                [challengelistcell.actionbutton setBackgroundImage:[UIImage imageNamed:@"btn_accept.png"] forState:UIControlStateNormal];
                [challengelistcell.actionbutton addTarget:self action:@selector(acceptChallenge:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        return challengelistcell;
    }
    else
    {
        static NSString *sectionTableIdentifier = @"SectionsbbTableIdentifier";
        
        challengeresultcell= (ChallengeResultCell*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
        
        if(challengeresultcell == nil)
        {
            challengeresultcell = [[[ChallengeResultCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
        }
        
        challengeresultcell.frame = CGRectMake(0, 0, self.tblChallenge.frame.size.width, TableRowHeight);
        challengeresultcell.selectionStyle = UITableViewCellSelectionStyleNone;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [tableView setSeparatorColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0]];
        
        if(challengeResult != nil || [challengeResult count] != 0)
        {
            NSDictionary* usinfo = [challengeResult objectAtIndex:indexPath.row];
            
            NSDictionary* usdetails = [usinfo objectForKey:@"user"];
            
            if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:userID])
            {
                challengeresultcell.usernamelabel.text = [usdetails objectForKey:@"splayer_username"];
                challengeresultcell.usermessagelabel.text = [usdetails objectForKey:@"splayer_msg"];
                tempID = [usdetails objectForKey:@"splayer_id"];
                
                if([[usdetails objectForKey:@"fplayer_comp_poss"] intValue] > [[usdetails objectForKey:@"splayer_comp_poss"] intValue])
                {
                    //user win
                    challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d scores more.", [[usdetails objectForKey:@"fplayer_comp_poss"] intValue] - [[usdetails objectForKey:@"splayer_comp_poss"] intValue]];
                }
                else if([[usdetails objectForKey:@"fplayer_comp_poss"] intValue] < [[usdetails objectForKey:@"splayer_comp_poss"] intValue])
                {
                    //user lose
                    challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d scores less.", [[usdetails objectForKey:@"splayer_comp_poss"] intValue] - [[usdetails objectForKey:@"fplayer_comp_poss"] intValue]];
                }
                else
                {
                    if([[usdetails objectForKey:@"fplayer_comp_time"] intValue] < [[usdetails objectForKey:@"splayer_comp_time"] intValue])
                    {
                        //user win
                        challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %ds faster.", [[usdetails objectForKey:@"splayer_comp_time"] intValue] - [[usdetails objectForKey:@"fplayer_comp_time"] intValue]];
                    }
                    else if([[usdetails objectForKey:@"fplayer_comp_time"] intValue] > [[usdetails objectForKey:@"splayer_comp_time"] intValue])
                    {
                        //user lose
                        challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %ds slower.", [[usdetails objectForKey:@"fplayer_comp_time"] intValue] - [[usdetails objectForKey:@"splayer_comp_time"] intValue]];
                    }
                    else
                    {
                        if([[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] < [[usdetails objectForKey:@"splayer_wrong_tried"] intValue])
                        {
                            //user win
                            challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d less wrong.", [[usdetails objectForKey:@"splayer_wrong_tried"] intValue] - [[usdetails objectForKey:@"fplayer_wrong_tried"] intValue]];
                        }
                        else if([[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] > [[usdetails objectForKey:@"splayer_wrong_tried"] intValue])
                        {
                            //user lose
                            challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d more wrong.", [[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] - [[usdetails objectForKey:@"splayer_wrong_tried"] intValue]];
                        }
                        else
                        {
                            if([[usdetails objectForKey:@"fplayer_repeat"] intValue] < [[usdetails objectForKey:@"splayer_repeat"] intValue])
                            {
                                //user win
                                challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d less repeat.", [[usdetails objectForKey:@"splayer_repeat"] intValue] - [[usdetails objectForKey:@"fplayer_repeat"] intValue]];
                            }
                            else if([[usdetails objectForKey:@"fplayer_repeat"] intValue] > [[usdetails objectForKey:@"splayer_repeat"] intValue])
                            {
                                //user lose
                                challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d more repeat.", [[usdetails objectForKey:@"fplayer_repeat"] intValue] - [[usdetails objectForKey:@"splayer_repeat"] intValue]];
                            }
                            else
                            {
                                //user draw
                                challengeresultcell.userresultlabel.text = @"Draw !!";
                            }
                            
                        }
                    }
                }
            }
            else
            {
                challengeresultcell.usernamelabel.text = [usdetails objectForKey:@"fplayer_username"];
                challengeresultcell.userresultlabel.text = @"";
                
                tempID = [usdetails objectForKey:@"fplayer_id"];
                
                if([[usdetails objectForKey:@"fplayer_comp_poss"] intValue] > [[usdetails objectForKey:@"splayer_comp_poss"] intValue])
                {
                    //user lose
                    challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d score less.", [[usdetails objectForKey:@"fplayer_comp_poss"] intValue] - [[usdetails objectForKey:@"splayer_comp_poss"] intValue]];
                }
                else if([[usdetails objectForKey:@"fplayer_comp_poss"] intValue] < [[usdetails objectForKey:@"splayer_comp_poss"] intValue])
                {
                    //user win
                    challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d score more.", [[usdetails objectForKey:@"splayer_comp_poss"] intValue] - [[usdetails objectForKey:@"fplayer_comp_poss"] intValue]];
                }
                else
                {
                    if([[usdetails objectForKey:@"fplayer_comp_time"] intValue] < [[usdetails objectForKey:@"splayer_comp_time"] intValue])
                    {
                        //user lose
                        challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %ds slower.", [[usdetails objectForKey:@"splayer_comp_time"] intValue] - [[usdetails objectForKey:@"fplayer_comp_time"] intValue]];
                    }
                    else if([[usdetails objectForKey:@"fplayer_comp_time"] intValue] > [[usdetails objectForKey:@"splayer_comp_time"] intValue])
                    {
                        //user win
                        challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %ds faster.", [[usdetails objectForKey:@"fplayer_comp_time"] intValue] - [[usdetails objectForKey:@"splayer_comp_time"] intValue]];
                    }
                    else
                    {
                        if([[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] < [[usdetails objectForKey:@"splayer_wrong_tried"] intValue])
                        {
                            //user lose
                            challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d more wrong.", [[usdetails objectForKey:@"splayer_wrong_tried"] intValue] - [[usdetails objectForKey:@"fplayer_wrong_tried"] intValue]];
                        }
                        else if([[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] > [[usdetails objectForKey:@"splayer_wrong_tried"] intValue])
                        {
                            //user win
                            challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d less wrong.", [[usdetails objectForKey:@"fplayer_wrong_tried"] intValue] - [[usdetails objectForKey:@"splayer_wrong_tried"] intValue]];
                        }
                        else
                        {
                            if([[usdetails objectForKey:@"fplayer_repeat"] intValue] < [[usdetails objectForKey:@"splayer_repeat"] intValue])
                            {
                                //user lose
                                challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You lose! %d more repeat.", [[usdetails objectForKey:@"splayer_repeat"] intValue] - [[usdetails objectForKey:@"fplayer_repeat"] intValue]];
                            }
                            else if([[usdetails objectForKey:@"fplayer_repeat"] intValue] > [[usdetails objectForKey:@"splayer_repeat"] intValue])
                            {
                                //user win
                                challengeresultcell.userresultlabel.text = [NSString stringWithFormat:@"You win! %d less repeat.", [[usdetails objectForKey:@"fplayer_repeat"] intValue] - [[usdetails objectForKey:@"splayer_repeat"] intValue]];
                            }
                            else
                            {
                                //user draw
                                challengeresultcell.userresultlabel.text = @"Draw !!";
                            }
                        }
                    }
                }
            }
            
            NSString *strurl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture", tempID];
            
            NSURL *url = [NSURL URLWithString:strurl];
            
            challengeresultcell.userprofileimage.imageURL = url;
            
            challengeresultcell.actionbutton.tag = indexPath.row + 200;
            [challengeresultcell.actionbutton setBackgroundImage:[UIImage imageNamed:@"btn_rechallenge.png"] forState:UIControlStateNormal];
            [challengeresultcell.actionbutton addTarget:self action:@selector(reChallenge:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return challengeresultcell;
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* headerView = nil;
    if(tableView == self.tblChallenge){
        if(challengeList.count != 0){
            headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 51)] autorelease];
            UIImageView *header = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_challenge_board.png"]] autorelease];
            [headerView addSubview:header];
        }
    }else{
        if(challengeResult.count != 0){
            headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 51)] autorelease];
            UIImageView *header = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bar_challenge_result.png"]] autorelease];
            [headerView addSubview:header];
        }
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 51.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableRowHeight;
}

- (void) retrieveResult
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [GlobalFunction report_memory];
            
            WebServices *ws = [[WebServices alloc] init];
            
            NSArray *arrUpdatedChallengeList = [ws selectChallengeByFBID:userID];
            
            NSArray *arrUpdatedChallengeResult = [ws selectChallengeResultByFBID:userID];
            
            [ws release];
            
            if((challengeResult.count != arrUpdatedChallengeResult.count) || (challengeList.count != arrUpdatedChallengeList.count))
            {
                if(challengeList.count != arrUpdatedChallengeList.count)
                {
                    challengeList = [arrUpdatedChallengeList copy];
                }
                
                if(challengeResult.count != arrUpdatedChallengeResult.count)
                {
                    /* filter here */
                    
                    NSMutableArray *test2 = [[NSMutableArray alloc] init];
                    
                    for(int i = 0; i < arrUpdatedChallengeResult.count; i++)
                    {
                        if(test2.count > 0)
                        {
                            NSDictionary* usinfo = [arrUpdatedChallengeResult objectAtIndex:i];
                            NSDictionary* usdetails = [usinfo objectForKey:@"user"];
                            
                            BOOL decider = true;
                            
                            for(int j = 0; j < test2.count; j++)
                            {
                                NSDictionary* usinfo2 = [test2 objectAtIndex:j];
                                NSDictionary* usdetails2 = [usinfo2 objectForKey:@"user"];
                                
                                if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:[usdetails2 objectForKey:@"fplayer_id"]])
                                {
                                    if([[usdetails objectForKey:@"splayer_id"] isEqualToString:[usdetails2 objectForKey:@"splayer_id"]])
                                    {
                                        if([[usdetails objectForKey:@"match_id"] intValue] >= [[usdetails2 objectForKey:@"match_id"] intValue])
                                        {
                                            //remove & add
                                            [test2 removeObjectAtIndex:j];
                                            [test2 addObject:[arrUpdatedChallengeResult objectAtIndex:i]];
                                            
                                            decider = false;
                                            
                                            break;
                                        }
                                        else
                                        {
                                            //since the 1 in array is the latest.. no need to remove & replace
                                        }
                                    }
                                    else
                                    {
                                        //decider = true;
                                        //[test2 addObject:[test objectAtIndex:i]];
                                    }
                                }
                                else
                                {
                                    if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:[usdetails2 objectForKey:@"splayer_id"]])
                                    {
                                        if([[usdetails objectForKey:@"splayer_id"] isEqualToString:[usdetails2 objectForKey:@"fplayer_id"]])
                                        {
                                            if([[usdetails objectForKey:@"match_id"] intValue] >= [[usdetails2 objectForKey:@"match_id"] intValue])
                                            {
                                                //remove & add
                                                
                                                [test2 removeObjectAtIndex:j];
                                                [test2 addObject:[arrUpdatedChallengeResult objectAtIndex:i]];
                                                
                                                decider = false;
                                                
                                                break;
                                            }
                                            else
                                            {
                                                //no replacement
                                            }
                                        }
                                        else
                                        {
                                            //decider = true;
                                            //[test2 addObject:[test objectAtIndex:i]];
                                        }
                                    }
                                    else
                                    {
                                        //decider = true;
                                        //[test2 addObject:[test objectAtIndex:i]];
                                    }
                                }
                            }
                            
                            if(decider)
                            {
                                [test2 addObject:[arrUpdatedChallengeResult objectAtIndex:i]];
                            }
                        }
                        else
                        {
                            [test2 addObject:[arrUpdatedChallengeResult objectAtIndex:i]];
                        }
                    }
                    
                    /* filter end */
                    
                    challengeResult = [test2 copy];
                    
                    [test2 release];
                }
                
                if(challengeList.count > 0){
                    self.tblChallenge.hidden = NO;
                    self.tblChallenge.frame = CGRectMake(15, 258, 290, 51 + 72 * challengeList.count);
                    if(challengeResult.count != 0){
                        self.tblResult.hidden = NO;
                        self.tblResult.frame = CGRectMake(15, 72 * challengeList.count + 319, 290, 51 + 72 * challengeResult.count);
                        self.svController.contentSize = CGSizeMake(320, 51 * 2 + 72 * (challengeList.count + challengeResult.count) + 278);
                    }
                    else{
                        self.svController.contentSize = CGSizeMake(320, 51 + 72 * challengeList.count + 268);
                    }
                }else{
                    if(challengeResult.count > 0){
                        self.tblResult.hidden = NO;
                        self.tblResult.frame = CGRectMake(15, 258, 290, 51 + 72 * challengeResult.count);
                        self.svController.contentSize = CGSizeMake(320, 51 + 72 * (challengeList.count + challengeResult.count) + 268);
                    }
                    else{
                        self.svController.contentSize = CGSizeMake(320, 258);
                    }
                }
                
                self.tblResult.scrollEnabled = NO;
                self.tblChallenge.scrollEnabled = NO;
                [tblChallenge reloadData];
                [tblResult reloadData];
            }
        });
    });
}

-(IBAction)setting:(id)sender{
    SettingScene *GtestClasssViewController=[[[SettingScene alloc] initWithNibName:@"SettingScene"  bundle:nil] autorelease];
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)functionTappedOwn:(id)sender
{
    [GlobalFunction LoadingScreen:self];
    [sender setUserInteractionEnabled:NO];
    
    if (FBSession.activeSession.isOpen)
    {
        [[FBRequest requestForMyFriends] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
         {
             
             if (!error)
             {
                 [GlobalFunction RemovingScreen:self];
                 NSArray *sortItem = [[[(NSDictionary*) user objectForKey:@"data"] retain] autorelease];
                 BOOL notExist = true;
                 
                 _fbfriendlist = [sortItem sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
                 
                 for(int i = 0; i < [_fbfriendlist count]; i++)
                 {
                     NSDictionary *friend = [_fbfriendlist objectAtIndex:i];
                     
                     for(int j = 0; j < [_userList count]; j++)
                     {
                         NSDictionary* usinfo = [_userList objectAtIndex:j];
                         
                         NSDictionary* usdetails = [usinfo objectForKey:@"user"];
                         
                         if([[friend objectForKey:@"id"] isEqualToString:[usdetails objectForKey:@"fb_id"]])
                         {
                             NSString *strTemp = [NSString stringWithFormat:@"%@%@", [usdetails objectForKey:@"score"], @" points"];
                             
                             [_matchedUser addObject:strTemp];
                             notExist = false;
                             break;
                         }
                     }
                     
                     if(notExist)
                     {
                         [_matchedUser addObject:@""];
                     }
                     else
                     {
                         notExist = true;
                     }
                 }
                 
             }
             [sender setUserInteractionEnabled:YES];
             
             FacebookFriendList *ffl = [[FacebookFriendList alloc] initWithNibName:@"FacebookFriendList" bundle:nil];
             
             ffl.arr = _fbfriendlist;
             ffl.matchedUser = _matchedUser;
             ffl.userScore = userScore;
             ffl.userID = userID;
             ffl.imageData = imageData;
             ffl.userName = userName;
             [self presentModalViewController:ffl animated:YES];
             [ffl release];
         }];
    }
}

-(IBAction)singleplayergame:(id)sender
{
    
    ReadyScene *GtestClasssViewController=[[[ReadyScene alloc] initWithNibName:@"ReadyScene"  bundle:nil] autorelease];
    GtestClasssViewController.gamemode = 1;
    GtestClasssViewController.userName = userName;
    GtestClasssViewController.userScore = userScore;
    GtestClasssViewController.userID = userID;
    GtestClasssViewController.imageData = imageData;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)acceptChallenge: (id)sender
{
    ReadyScene *GtestClasssViewController=[[[ReadyScene alloc] initWithNibName:@"ReadyScene"  bundle:nil] autorelease];
    GtestClasssViewController.gamemode = 2;
    GtestClasssViewController.gameplayer = 2;
    GtestClasssViewController.userName = userName;
    GtestClasssViewController.userScore = userScore;
    GtestClasssViewController.userID = userID;
    GtestClasssViewController.imageData = imageData;
    
    //pass the gameplay_id as well
    NSDictionary* usinfo = [self.challengeList objectAtIndex:[sender tag] - 100];
    NSDictionary* usdetails = [usinfo objectForKey:@"user"];
    
    GtestClasssViewController.gameplayid = [[usdetails objectForKey:@"gameplay_id"] intValue];
    
    //pass fplayer combo, repeat, wrong, time
    GtestClasssViewController.fstrWrong = [usdetails objectForKey:@"fplayer_wrong_tried"];
    GtestClasssViewController.fstrRepeat = [usdetails objectForKey:@"fplayer_repeat"];
    GtestClasssViewController.fcorrect = [usdetails objectForKey:@"fplayer_comp_poss"];
    GtestClasssViewController.fcompleteTime = [usdetails objectForKey:@"fplayer_comp_time"];
    GtestClasssViewController.splayerID = [usdetails objectForKey:@"fplayer_id"];
    GtestClasssViewController.splayerUsername = [usdetails objectForKey:@"fplayer_username"];
    GtestClasssViewController.match_id = [[usdetails objectForKey:@"match_id"] intValue];
    GtestClasssViewController.fmessage = [usdetails objectForKey:@"fplayer_msg"];
    
    GtestClasssViewController.isshow = YES;
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(IBAction)reChallenge: (id)sender
{
    ReadyScene *GtestClasssViewController=[[[ReadyScene alloc] initWithNibName:@"ReadyScene"  bundle:nil] autorelease];
    GtestClasssViewController.gamemode = 2;
    GtestClasssViewController.gameplayer = 1;
    GtestClasssViewController.userName = userName;
    GtestClasssViewController.userScore = userScore;
    GtestClasssViewController.userID = userID;
    GtestClasssViewController.imageData = imageData;
    
    NSDictionary* usinfo = [self.challengeResult objectAtIndex:[sender tag] - 200];
    NSDictionary* usdetails = [usinfo objectForKey:@"user"];
    
    if([[usdetails objectForKey:@"fplayer_id"] isEqualToString:userID])
    {
        GtestClasssViewController.splayerID = [usdetails objectForKey:@"splayer_id"];
        GtestClasssViewController.splayerUsername = [usdetails objectForKey:@"splayer_username"];
    }
    else
    {
        GtestClasssViewController.splayerID = [usdetails objectForKey:@"fplayer_id"];
        GtestClasssViewController.splayerUsername = [usdetails objectForKey:@"fplayer_username"];
    }
    
    [self presentModalViewController:GtestClasssViewController animated:YES];
}

-(void)guideviewanimationin
{
    [refreshTimer invalidate];
    [connectiontimer invalidate];
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.2];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation([[UIScreen mainScreen] bounds].size.width, 0);
        self.guideview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             guidetransform = self.guideview.transform;
                         }
                     }];
    [UIView commitAnimations];
}

-(void)guideviewanimationout
{
    self.guideview.transform = guidetransform;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.2];
    [UIView animateWithDuration:0.0 animations:^{
        CGAffineTransform translate = CGAffineTransformMakeTranslation(-[[UIScreen mainScreen] bounds].size.width, 0);
        self.guideview.transform = translate;
    }
                     completion:^(BOOL finished){
                         if (finished) {
                             guidetransform = self.guideview.transform;
                             refreshTimer = [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(retrieveResult) userInfo:nil repeats:YES];
                             connectiontimer = [NSTimer scheduledTimerWithTimeInterval:(0.1) target:self selector:@selector(checkingconnection) userInfo:nil repeats:YES];
                         }
                     }];
    [UIView commitAnimations];
}

- (void)rightSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(guidepage != 1)
    {
        guidepage--;
        CGRect frame = self.guidescroll.frame;
        frame.origin.x = frame.size.width * (guidepage - 1);
        frame.origin.y = 0;
        [self.guidescroll scrollRectToVisible:frame animated:YES];
    }
}

- (void)leftSwipeHandle:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(guidepage != 5)
    {
        guidepage++;
        CGRect frame = self.guidescroll.frame;
        frame.origin.x = frame.size.width * (guidepage - 1);
        frame.origin.y = 0;
        [self.guidescroll scrollRectToVisible:frame animated:YES];
    }
}

-(IBAction)closeguideview:(id)sender
{
    [self guideviewanimationout];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isshow = NO;
}

@end
