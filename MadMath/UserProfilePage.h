//
//  UserProfilePage.h
//  MadMath
//
//  Created by Jermin Bazazian on 9/10/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <mach/mach.h>
#import "Facebook.h"
#import "AppDelegate.h"
#import "LoginPage.h"
#import "WebServices.h"
#import "FacebookFriendList.h"
#import "ReadyScene.h"
#import <QuartzCore/QuartzCore.h>
#import "GlobalFunction.h"
#import "LoginPage.h"
#import "SettingScene.h"
#import "Toast+UIView.h"
#import "ChallengeListCell.h"
#import "ChallengeResultCell.h"
#import "AsyncImageView.h"

@interface UserProfilePage : UIViewController<UITableViewDataSource, FBDialogDelegate, FBRequestDelegate, UITableViewDelegate>
{
    ChallengeListCell *challengelistcell;
    ChallengeResultCell *challengeresultcell;
    
    NSTimer *connectiontimer, *refreshTimer;
    BOOL isconnected, isfirsttime, isneedtorefresh;
    int countdown, countinterrupted, guidepage;
    CGAffineTransform guidetransform;
    UIPageControl *pageControl;
    
    NSMutableArray *latestarraylist, *latestarray, *iamfirst, *iamsecond, *iamfirstlist, *iamsecondlist;
}

@property (retain, nonatomic) IBOutlet UIScrollView *svController;
@property (retain, nonatomic) IBOutlet UITableView *tblChallenge;
@property (retain, nonatomic) IBOutlet UITableView *tblResult;
@property (retain, nonatomic) IBOutlet UILabel *lblScore;
@property (retain, nonatomic) IBOutlet UIImageView *imgUser;
@property (retain, nonatomic) IBOutlet UILabel *lblWelcome;
@property (retain, nonatomic) IBOutlet UIImageView *titleimageview;

@property (retain, nonatomic) NSString *userScore;
@property (retain, nonatomic) NSArray *fbfriendlist;
@property (retain, nonatomic) NSArray *userList;
@property (retain, nonatomic) NSMutableArray *matchedUser;
@property (retain, nonatomic) NSString *userID;
@property (retain, nonatomic) NSString *userName;
@property (retain, nonatomic) NSData *imageData;
@property (retain, nonatomic) NSArray *challengeResult;
@property (retain, nonatomic) NSArray *challengeList;

@property (nonatomic) BOOL isneedtoplay;

-(IBAction)singleplayergame:(id)sender;
-(IBAction)functionTappedOwn:(id)sender;
-(IBAction)setting:(id)sender;

@property (retain, nonatomic) IBOutlet UIView *guideview;
@property (retain, nonatomic) IBOutlet UIScrollView *guidescroll;
@property (retain, nonatomic) IBOutlet UIButton *guidebutton;

-(IBAction)closeguideview:(id)sender;
@end