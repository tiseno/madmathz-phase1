//
//  SettingScene.m
//  MadMath
//
//  Created by Jermin Bazazian on 9/18/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "SettingScene.h"

@implementation SettingScene

@synthesize bgmusicswitch, soundswitch, bglabel, soundlabel, guidelabel, tipslabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([[UIScreen mainScreen] bounds].size.height > 480)
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default_4in.png"]];
    }
    else
    {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background_default.png"]];
    }
    
    ischange = NO;
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if(appDelegate.ismute)
    {
        [soundswitch setOn:NO animated:YES];
    }
    else
    {
        [soundswitch setOn:YES animated:YES];
    }
    
    if(appDelegate.volume > 0.0)
    {
        [bgmusicswitch setOn:YES animated:YES];
    }
    else
    {        
        [bgmusicswitch setOn:NO animated:YES];
    }
    
    self.bglabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.soundlabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.guidelabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.tipslabel.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
    self.bglabel.font = [UIFont fontWithName:@"Adonais" size:25];
    self.soundlabel.font = [UIFont fontWithName:@"Adonais" size:25];
    self.guidelabel.font = [UIFont fontWithName:@"Adonais" size:25];
    self.tipslabel.font = [UIFont fontWithName:@"Adonais" size:25];
    
    db = [[DatabaseAction alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    countinterrupted = 0;
    timer = [NSTimer scheduledTimerWithTimeInterval:(0.5) target:self selector:@selector(musicvalue) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [db updateBGMusic:appDelegate.volume];
    [db updateSound:(int)appDelegate.ismute];
}

-(void)dealloc{
    [bgmusicswitch release];
    [soundswitch release];
    [bglabel release];
    [soundlabel release];
    [guidelabel release];
    [tipslabel release];
    
    [db release];
    
    [super dealloc];
}

-(void)musicvalue
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    if(bgmusicswitch.on)
        appDelegate.volume = 1.0;
    else
        appDelegate.volume = 0.0;
    [GlobalFunction setvolumebgmusic:appDelegate.volume];
    
    if(soundswitch.on)
        appDelegate.ismute = NO;
    else
        appDelegate.ismute = YES;
    
    if(![GlobalFunction isplay])
    {
        ++countinterrupted;
        if(countinterrupted < 4)
            [GlobalFunction resumebgmusic];
        else if(countinterrupted == 4)
            [self.view makeToast:@"Background Music has been interrupted." duration:(0.5) position:@"center"];
    }
}

-(IBAction)logoutButtonWasPressed:(id)sender
{
    [self alertview];
}

-(IBAction)back:(id)sender{
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)guide:(id)sender
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.isshow = YES;
    [super dismissModalViewControllerAnimated:YES];
}

-(IBAction)tips:(id)sender
{
    NSURL *url = [[ [ NSURL alloc ] initWithString: @"http://www.squarefresco.com" ] autorelease];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)alertview{
    alert = [[[UIAlertView alloc] initWithTitle:@"Sign Out From Facebook" message:@"Are You Sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] autorelease];
    UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"alert_box.png"]] autorelease];
    
    backgroundImageView.frame = CGRectMake(0, 0, 282, 135);
    [alert addSubview:backgroundImageView];
    
    [alert sendSubviewToBack:backgroundImageView];
    [alert show];
    
    UILabel *theTitle = [alert valueForKey:@"_titleLabel"];
    [theTitle setTextColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0]];
    
    UILabel *theBody = [alert valueForKey:@"_bodyTextLabel"];
    [theBody setTextColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0]];

    UIButton *theButton = [alert valueForKey:@"_buttons"];
    [[theButton objectAtIndex:0] setTitleColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
    [[theButton objectAtIndex:1] setTitleColor:[UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0] forState:UIControlStateNormal];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        [alert dismissWithClickedButtonIndex:0 animated:NO];
    }else{
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        [FBSession.activeSession closeAndClearTokenInformation];
    }
}

@end
