//
//  ChallengeResultCell.h
//  MadMath
//
//  Created by tiseno on 12/7/12.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ChallengeResultCell : UITableViewCell
{
    
}

@property (retain, nonatomic) UIImageView *userprofileimage;
@property (retain, nonatomic) UILabel *usernamelabel, *usermessagelabel, *userresultlabel;
@property (retain, nonatomic) UIButton *actionbutton;

@end
