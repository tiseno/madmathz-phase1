//
//  ChallengeListCell.m
//  MadMath
//
//  Created by tiseno on 12/7/12.
//
//

#import "ChallengeListCell.h"

@implementation ChallengeListCell

@synthesize userprofileimage, usernamelabel, usermessagelabel, userplaytimelabel, actionbutton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UIImageView *userimage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        userimage.layer.masksToBounds = YES;
        userimage.layer.cornerRadius = 5.0;
        userimage.layer.borderColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0].CGColor;
        userimage.layer.borderWidth = 1.5;
        self.userprofileimage = userimage;
        [userimage release];
        [self addSubview:userprofileimage];
        
        UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(66, 3, 115, 21)];
        userName.textAlignment = UITextAlignmentLeft;
        userName.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
        userName.backgroundColor = [UIColor clearColor];
        userName.font = [UIFont boldSystemFontOfSize:14];
        userName.lineBreakMode = UILineBreakModeWordWrap;
        userName.numberOfLines = 0;
        self.usernamelabel = userName;
        [userName release];
        [self addSubview:usernamelabel];
        
        UILabel *userMessage = [[UILabel alloc] initWithFrame:CGRectMake(66, 20, 122, 32)];
        userMessage.textAlignment = UITextAlignmentLeft;
        userMessage.textColor = [UIColor colorWithRed:229.0/255.0 green:81.0/255.0 blue:15.0/255.0 alpha:1.0];
        userMessage.backgroundColor = [UIColor clearColor];
        userMessage.font = [UIFont systemFontOfSize:11];
        userMessage.lineBreakMode = UILineBreakModeWordWrap;
        userMessage.numberOfLines = 0;
        self.usermessagelabel = userMessage;
        [userMessage release];
        [self addSubview:usermessagelabel];
        
        UILabel *userTimeplayed = [[UILabel alloc] initWithFrame:CGRectMake(66, 45, 122, 21)];
        userTimeplayed.textAlignment = UITextAlignmentLeft;
        userTimeplayed.textColor = [UIColor colorWithRed:90.0/255.0 green:66.0/255.0 blue:0 alpha:1.0];
        userTimeplayed.backgroundColor = [UIColor clearColor];
        userTimeplayed.font = [UIFont systemFontOfSize:11];
        userTimeplayed.lineBreakMode = UILineBreakModeWordWrap;
        userTimeplayed.numberOfLines = 1;
        self.userplaytimelabel = userTimeplayed;
        [userTimeplayed release];
        [self addSubview:userplaytimelabel];
        
        UIButton *actionButton = [[[UIButton alloc]init] autorelease];
        actionButton.frame = CGRectMake(195.0f, 5.0f, 90.0f, 53.0f);
        actionButton.backgroundColor = [UIColor clearColor];
        self.actionbutton = actionButton;
        [self addSubview:actionbutton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc
{
    [userprofileimage release];
    [usernamelabel release];
    [usermessagelabel release];
    [userplaytimelabel release];
    [actionbutton release];
    
    [super dealloc];
}

@end
