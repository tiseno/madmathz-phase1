//
//  SettingScene.h
//  MadMath
//
//  Created by Jermin Bazazian on 9/18/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Facebook.h"
#import "AppDelegate.h"
#import "UserProfilePage.h"
#import "DatabaseAction.h"

@interface SettingScene : UIViewController<UIAlertViewDelegate>{
    NSTimer *timer;
    BOOL ischange;
    float currentvolume, aftervolume;
    int countinterrupted;
    UIAlertView *alert;
    
    DatabaseAction *db;
}

@property (nonatomic, retain) IBOutlet UISwitch *bgmusicswitch;
@property (nonatomic, retain) IBOutlet UISwitch *soundswitch;
@property (nonatomic, retain) IBOutlet UILabel *bglabel;
@property (nonatomic, retain) IBOutlet UILabel *soundlabel;
@property (nonatomic, retain) IBOutlet UILabel *guidelabel;
@property (nonatomic, retain) IBOutlet UILabel *tipslabel;

-(IBAction)logoutButtonWasPressed:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)guide:(id)sender;
-(IBAction)tips:(id)sender;

@end
